package sg.gov.dsta.sft.enums;

/**
 * 
 * @author Asheek
 * @version 1.0
 */

public enum ProcessingSteps {

  SCANNING("Scanning"), PUT_PRESIGNED_URL("PutPreSignedUrl"), GETPRESIGNED_URL(
      "GetPreSignedUrl"), FILE_SCAN_RESULT("FileScanResult");


  private final String processStep;

  ProcessingSteps(String processStep) {

    this.processStep = processStep;
  }


  public String getProcessingStep() {
    return this.processStep;
  }


}
