package sg.gov.dsta.sft.enums;

/**
 * 
 * @author Asheek
 * @version 1.0
 */
public enum FileStatus {

  DEFAULT(""),
  UNCHANGED("Unchanged"), 
  SANITIZED("Sanitized"), 
  QUARANTINED("Quarantined"), 
  INTERNAL_PROCESSING_ERROR("Internal Processing Error");

  private final String status;

  FileStatus(String status) {

    this.status = status;
  }

  public String getFileStatus() {
    return this.status;
  }

}
