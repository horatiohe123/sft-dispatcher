package sg.gov.dsta.sft.enums;
/**
 * 
 * @author Asheek
 * @version 1.0
 */
public enum ScanVerdict {

  // https://docs.opswat.com/mdcore/metadefender-core/ref#fileanalysisget

  NO_THREAT_DETECTED(0,"No Threat Detected"), 
  INFECTED(1,"Infected"), 
  SUSPICIOUS(2,"Suspicious"),
  FAILED(3,"Failed"), 
  CLEANED_DELETED(4,"Cleaned / Deleted"),
  SCAN_SKIPPED_WHITELISTED(7,"Scan Skipped - Whitelisted"),
  SCAN_SKIPPED_BLACKLISTED(8,"Scan Skipped - Blacklisted"), 
  EXCEEDED_ARCHIVE_DEPTH(9,"Exceeded Archive Depth"), 
  NOT_SCANNED(10,"Not Scanned"),
  ENCRYPTED_ARCHIVE(12,"Encrypted Archive"), 
  EXCEEDED_ARCHIVE_SIZE(13,"Exceeded Archive Size"), 
  EXCEEDED_ARCHIVE_FILE_NUMBER(14,"Exceeded Archive File Number"),
  PASSWORD_PROTECTED_DOCUMENT(15,"Password Protected Document"), 
  EXCEEDED_ARCHIVE_TIMEOUT(16,"Exceeded Archive Timeout"), 
  FILE_TYPE_MISMATCH(17,"File type Mismatch"),
  POTENTIALLY_VULNERABLE_FILE(18,"Potentially Vulnerable File"), 
  CANCELED(19,"Canceled"), 
  SENSITIVE_DATA_FOUND(20,"Sensitive data found"),
  YARA_RULE_MATCH(21,"Yara Rule Matched"), 
  POTENTIALLY_UNWANTED_PROGRAM(22,"Potentially Unwanted Program"), 
  UNSUPPORTED_FILE_TYPE(23,"Unsupported file type"),
  IN_PROGRESS(255,"In Progress"),
  ; 


  private final int scanVerdictCode;
  private final String scanVerdictDescription;

  ScanVerdict(int scanResultCode, String scanDescription) {
    this.scanVerdictCode = scanResultCode;
    this.scanVerdictDescription=scanDescription;
  }

  public int getScanVerdictCode() {
    return this.scanVerdictCode;
  }
  
  public String getScanVerdictDescrtiption() {
    return this.scanVerdictDescription;
  }

}
