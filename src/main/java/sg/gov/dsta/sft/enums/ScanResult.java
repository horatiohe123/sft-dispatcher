package sg.gov.dsta.sft.enums;

/**
 * 
 * @author Asheek
 * @version 1.0
 */
public enum ScanResult {

  // https://docs.opswat.com/mdcore/metadefender-core/ref#fileanalysisget


  ALLOWED("Allowed"), BLOCKED("Blocked"), PROCESSING("Processing");


  private final String scanResults;

  ScanResult(String scanResults) {

    this.scanResults = scanResults;
  }


  public String getScanResult() {
    return this.scanResults;
  }

}
