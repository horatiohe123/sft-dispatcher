package sg.gov.dsta.ecs.sft.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jboss.logging.Logger;
import sg.gov.dsta.ecs.sft.aws.DynamoDBHandler;
import sg.gov.dsta.ecs.sft.dao.EventStoreObject;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.Map;

/**
 * Store all events for microservice
 */
@ApplicationScoped
public class EventStoreService {
    private static final Logger log = Logger.getLogger(EventStoreService.class);
    private static final String LOG_FORMAT = "[SFT EventStore]: %s";
    private static final String TABLE_NAME_TEMPLATE = "sft-eventstore";
    private String env;
    private String tableName;
    private final ObjectMapper mapper = new ObjectMapper();

    @Inject
    public DynamoDBHandler ddh;

    public EventStoreService() {
        log.infof(LOG_FORMAT, "initializing...");
        env = System.getenv("ENVIRONMENT");
        if (env != null) {
            tableName = TABLE_NAME_TEMPLATE + "-" + env;
        } else {
            tableName = TABLE_NAME_TEMPLATE;
        }
    }

    public void addEvent(EventStoreObject eso) {
        ddh.putItem(tableName, eso.toMap()); // Added event to data store
        log.infof(LOG_FORMAT, eso.getProcessingStatus()); // Added event to logs
    }

    public EventStoreObject getEvent(String pk) {
        Map<String, Object> map = ddh.getItem(tableName, pk, null);
        if (!map.isEmpty()) {
            return mapper.convertValue(map, EventStoreObject.class);
        } else {
            return null;
        }
    }

    public EventStoreObject updateEvent(String pk, EventStoreObject eso) {
        return updateEvent(pk, mapper.convertValue(eso, Map.class));
    }

    public EventStoreObject updateEvent(String pk, Map<String, Object> updates) {
        updates.remove("pk");
        Map<String, Object> map = ddh.updateTableItems(tableName, pk, null, updates);
        if (!map.isEmpty()) {
            return mapper.convertValue(map, EventStoreObject.class);
        } else {
            return null;
        }
    }
}
