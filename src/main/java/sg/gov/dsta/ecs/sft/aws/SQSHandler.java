package sg.gov.dsta.ecs.sft.aws;

import org.jboss.logging.Logger;
import sg.gov.dsta.ecs.sft.utils.CustomLogger;
import software.amazon.awssdk.http.urlconnection.UrlConnectionHttpClient;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.sqs.SqsClient;
import software.amazon.awssdk.services.sqs.model.CreateQueueRequest;
import software.amazon.awssdk.services.sqs.model.GetQueueUrlRequest;
import software.amazon.awssdk.services.sqs.model.SendMessageRequest;
import software.amazon.awssdk.services.sqs.model.SendMessageResponse;
import software.amazon.awssdk.services.sqs.model.SqsException;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class SQSHandler {
    private static final Logger log = Logger.getLogger("SQSHandlerV2");

    public SqsClient sqsClient;

    public SQSHandler() {
        Region region = Region.AP_SOUTHEAST_1;
        sqsClient = SqsClient.builder()
                .region(region)
                .httpClient(UrlConnectionHttpClient.builder().build())
                .build();
    }

    public void sendMessage(String queueName, String message, String uuid, String msguuid) {
        long inTimestamp = System.currentTimeMillis();
        try {
            CreateQueueRequest request = CreateQueueRequest.builder()
                    .queueName(queueName)
                    .build();
            sqsClient.createQueue(request);

            GetQueueUrlRequest getQueueRequest = GetQueueUrlRequest.builder()
                    .queueName(queueName)
                    .build();

            String queueUrl = sqsClient.getQueueUrl(getQueueRequest).queueUrl();
            SendMessageRequest sendMsgRequest = SendMessageRequest.builder()
                    .queueUrl(queueUrl)
                    .messageBody(message)
                    .delaySeconds(1)
                    .build();

            SendMessageResponse sendMessageResponse = sqsClient.sendMessage(sendMsgRequest);
            log.info("[SQSHandler Send]: " + sendMessageResponse.toString());

            long inTimestamp1 = System.currentTimeMillis() - inTimestamp;
            log.info(CustomLogger.toJson(new CustomLogger("SFT", "", "[Dispatcher]", "[SQSHandler]",
                    "/v1/ecs/sft/", uuid, msguuid, "INFO", "[Message Processing]", "Message has been delivered successfully in the queue - " + queueName, "", inTimestamp1)));
            log.info("[SQS Handler]: message is sent successfully to " + queueName);

        } catch (SqsException e) {
            log.error("[SQSHandler Send]: " + e.awsErrorDetails().errorMessage());
        }
    }
}
