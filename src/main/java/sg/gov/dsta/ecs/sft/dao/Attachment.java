package sg.gov.dsta.ecs.sft.dao;

import io.quarkus.runtime.annotations.RegisterForReflection;

import java.util.Objects;

@RegisterForReflection
public class Attachment {
    private String name, type, content;

    public Attachment() {
    }

    public Attachment(String name, String type, String content) {
        this.name = name;
        this.type = type;
        this.content = content;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Attachment name(String name) {
        setName(name);
        return this;
    }

    public Attachment type(String type) {
        setType(type);
        return this;
    }

    public Attachment content(String content) {
        setContent(content);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Attachment)) {
            return false;
        }
        Attachment attachment = (Attachment) o;
        return Objects.equals(name, attachment.name) && Objects.equals(type, attachment.type) && Objects.equals(content, attachment.content);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, type, content);
    }

    @Override
    public String toString() {
        return "{" +
                " name='" + getName() + "'" +
                ", type='" + getType() + "'" +
                ", content='" + getContent() + "'" +
                "}";
    }

}
