package sg.gov.dsta.ecs.sft.dao;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.quarkus.runtime.annotations.RegisterForReflection;

@JsonIgnoreProperties(ignoreUnknown = true)
@RegisterForReflection
public class ResponseMessage {

  @JsonProperty("Uuid")
  String uuid;
  @JsonProperty("ProcessingStep")
  String processingStep;
  @JsonProperty("ProcessingStatus")
  String processingStatus;
  @JsonProperty("PreSignedUrl")
  String presignUrl;
  @JsonProperty("Verdicts")
  String verdicts;
  @JsonProperty("FileStatus")
  String fileStatus;

  private ResponseMessage(Builder builder) {
    this.uuid = builder.uuid;
    this.processingStep = builder.processingStep;
    this.processingStatus = builder.processingStatus;
    this.presignUrl = builder.presignUrl;
    this.verdicts = builder.verdicts;
    this.fileStatus = builder.fileStatus;
  }

  public ResponseMessage() {}

  public String getUuid() {
    return uuid;
  }

  public String getProcessingStep() {
    return processingStatus;
  }

  public String getProcessingStatus() {
    return processingStep;
  }

  public String getPresignUrl() {
    return presignUrl;
  }

  public String getVerdicts() {
    return verdicts;
  }

  public String getFileStatus() {
    return fileStatus;
  }

  public void setUuid(String uuid) {
    this.uuid = uuid;
  }

  public void setProcessingStep(String processingStep) {
    this.processingStep = processingStep;
  }

  public void setProcessingStatus(String processingStatus) {
    this.processingStatus = processingStatus;
  }

  public void setPresignUrl(String presignUrl) {
    this.presignUrl = presignUrl;
  }

  public void setVerdicts(String verdicts) {
    this.verdicts = verdicts;
  }

  public void setFileStatus(String fileStatus) {
    this.fileStatus = fileStatus;
  }

  @Override
  public String toString() {
    return "ResponseMessage [uuid=" + uuid + ", processingStep=" + processingStep
        + ", processingStatus=" + processingStatus + ", presignUrl=" + presignUrl + ", verdicts="
        + verdicts + ", fileStatus=" + fileStatus + "]";
  }

  /**
   * Creates builder to build {@link ResponseMessage}.
   * 
   * @return created builder
   */
  public static Builder builder() {
    return new Builder();
  }

  /**
   * Builder to build {@link ResponseMessage}.
   */
  public static final class Builder {
    private String uuid;
    private String processingStep;
    private String processingStatus;
    private String presignUrl;
    private String verdicts;
    private String fileStatus;

    private Builder() {}

    public Builder withUuid(String uuid) {
      this.uuid = uuid;
      return this;
    }

    public Builder withProcessingStep(String processingStep) {
      this.processingStep = processingStep;
      return this;
    }

    public Builder withProcessingStatus(String processingStatus) {
      this.processingStatus = processingStatus;
      return this;
    }

    public Builder withPresignUrl(String presignUrl) {
      this.presignUrl = presignUrl;
      return this;
    }

    public Builder withVerdicts(String verdicts) {
      this.verdicts = verdicts;
      return this;
    }

    public Builder withFileStatus(String fileStatus) {
      this.fileStatus = fileStatus;
      return this;
    }

    public ResponseMessage build() {
      return new ResponseMessage(this);
    }
  }



}
