package sg.gov.dsta.ecs.sft.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.quarkus.runtime.annotations.RegisterForReflection;
import org.jboss.logging.Logger;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Objects;

@RegisterForReflection
public class CustomLogger {
    private String ApplicationName;
    private String SourceApplicationName;
    private String ModuleName;
    private String FunctionName;
    private String Endpoint;
    private String CorrelationID;
    private String UUID;
    private String LogType;
    private String Message;
    private String MessageDetails;
    private String Miscellaneous;
    private String Timestamp;
    private long duration;
    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static final Logger log = Logger.getLogger(CustomLogger.class);

    public static String toJson(CustomLogger logger) {
        ObjectMapper mapper = new ObjectMapper();
        String jo = null;
        try {
            jo = mapper.writeValueAsString(logger);
        } catch (JsonProcessingException e) {
            // TODO Auto-generated catch block
            log.error("[MCNS Custom Logger]: " + e.getMessage());
            e.printStackTrace();
        }
        return jo;
    }

    public CustomLogger() {
        this.Timestamp = sdf.format(new Timestamp(System.currentTimeMillis()));
    }

    public CustomLogger(String ApplicationName, String SourceApplicationName, String ModuleName, String FunctionName, String Endpoint, String CorrelationID, String UUID, String LogType, String Message, String MessageDetails, String Miscellaneous, long duration) {
        this.ApplicationName = ApplicationName;
        this.SourceApplicationName = SourceApplicationName;
        this.ModuleName = ModuleName;
        this.FunctionName = FunctionName;
        this.Endpoint = Endpoint;
        this.CorrelationID = CorrelationID;
        this.UUID = UUID;
        this.LogType = LogType;
        this.Message = Message;
        this.MessageDetails = MessageDetails;
        this.Miscellaneous = Miscellaneous;
        this.duration = duration;
        this.Timestamp = sdf.format(new Timestamp(System.currentTimeMillis()));
    }

    public Map<String, Object> toMap() {
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> map = mapper.convertValue(this, Map.class); // convert class to a map
        map.put("pk", getCorrelationID());
        map.put("sk", getUUID());
        return map;
    }

    public String getApplicationName() {
        return ApplicationName;
    }

    public void setApplicationName(String applicationName) {
        ApplicationName = applicationName;
    }

    public String getSourceApplicationName() {
        return SourceApplicationName;
    }

    public void setSourceApplicationName(String sourceApplicationName) {
        SourceApplicationName = sourceApplicationName;
    }

    public String getModuleName() {
        return ModuleName;
    }

    public void setModuleName(String moduleName) {
        ModuleName = moduleName;
    }

    public String getFunctionName() {
        return FunctionName;
    }

    public void setFunctionName(String functionName) {
        FunctionName = functionName;
    }

    public String getEndpoint() {
        return Endpoint;
    }

    public void setEndpoint(String endpoint) {
        Endpoint = endpoint;
    }

    public String getCorrelationID() {
        return CorrelationID;
    }

    public void setCorrelationID(String correlationID) {
        CorrelationID = correlationID;
    }

    public String getUUID() {
        return UUID;
    }

    public void setUUID(String UUID) {
        this.UUID = UUID;
    }

    public String getLogType() {
        return LogType;
    }

    public void setLogType(String logType) {
        LogType = logType;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getMessageDetails() {
        return MessageDetails;
    }

    public void setMessageDetails(String messageDetails) {
        MessageDetails = messageDetails;
    }

    public String getMiscellaneous() {
        return Miscellaneous;
    }

    public void setMiscellaneous(String miscellaneous) {
        Miscellaneous = miscellaneous;
    }

    public String getTimestamp() {
        return Timestamp;
    }

    public void setTimestamp(String timestamp) {
        Timestamp = sdf.format(timestamp);
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }



    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof CustomLogger)) {
            return false;
        }
        CustomLogger customLogger = (CustomLogger) o;
        return Objects.equals(ApplicationName, customLogger.ApplicationName) && Objects.equals(SourceApplicationName, customLogger.SourceApplicationName) && Objects.equals(ModuleName, customLogger.ModuleName) && Objects.equals(FunctionName, customLogger.FunctionName) && Objects.equals(Endpoint, customLogger.Endpoint) && Objects.equals(CorrelationID, customLogger.CorrelationID) && Objects.equals(UUID, customLogger.UUID) && Objects.equals(LogType, customLogger.LogType) && Objects.equals(Message, customLogger.Message) && Objects.equals(MessageDetails, customLogger.MessageDetails) && Objects.equals(Miscellaneous, customLogger.Miscellaneous) && Objects.equals(Timestamp, customLogger.Timestamp) && duration == customLogger.duration;
    }

    @Override
    public int hashCode() {
        return Objects.hash(ApplicationName, SourceApplicationName, ModuleName, FunctionName, Endpoint, CorrelationID, UUID, LogType, Message, MessageDetails, Miscellaneous, Timestamp, duration);
    }


    @Override
    public String toString() {
        return "{" +
                " ApplicationName='" + getApplicationName() + "'" +
                ", SourceAppliationName='" + getSourceApplicationName() + "'" +
                ", ModuleName='" + getModuleName() + "'" +
                ", FunctionName='" + getFunctionName() + "'" +
                ", Endpoint='" + getEndpoint() + "'" +
                ", CorrelationID='" + getCorrelationID() + "'" +
                ", UUID='" + getUUID() + "'" +
                ", LogType='" + getLogType() + "'" +
                ", Message='" + getMessage() + "'" +
                ", MessageDetails='" + getMessageDetails() + "'" +
                ", Miscellaneous='" + getMiscellaneous() + "'" +
                ", Timestamp='" + getTimestamp() + "'" +
                ", duration='" + getDuration() + "'" +
                "}";
    }


    public static final class Builder {
        private CustomLogger customLogger;

        private Builder() {
            customLogger = new CustomLogger();
        }

        public static Builder aCustomLogger() {
            return new Builder();
        }

        public Builder withApplicationName(String ApplicationName) {
            customLogger.setApplicationName(ApplicationName);
            return this;
        }

        public Builder withSourceApplicationName(String SourceApplicationName) {
            customLogger.setSourceApplicationName(SourceApplicationName);
            return this;
        }

        public Builder withModuleName(String ModuleName) {
            customLogger.setModuleName(ModuleName);
            return this;
        }

        public Builder withFunctionName(String FunctionName) {
            customLogger.setFunctionName(FunctionName);
            return this;
        }

        public Builder withEndpoint(String Endpoint) {
            customLogger.setEndpoint(Endpoint);
            return this;
        }

        public Builder withCorrelationID(String CorrelationID) {
            customLogger.setCorrelationID(CorrelationID);
            return this;
        }

        public Builder withUUID(String UUID) {
            customLogger.setUUID(UUID);
            return this;
        }

        public Builder withLogType(String LogType) {
            customLogger.setLogType(LogType);
            return this;
        }

        public Builder withMessage(String Message) {
            customLogger.setMessage(Message);
            return this;
        }

        public Builder withMessageDetails(String MessageDetails) {
            customLogger.setMessageDetails(MessageDetails);
            return this;
        }

        public Builder withMiscellaneous(String Miscellaneous) {
            customLogger.setMiscellaneous(Miscellaneous);
            return this;
        }

        public Builder withDuration(long duration) {
            customLogger.setDuration(duration);
            return this;
        }

        public CustomLogger build() {
            return customLogger;
        }
    }
}