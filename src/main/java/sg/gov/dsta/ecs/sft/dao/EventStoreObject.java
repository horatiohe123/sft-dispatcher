package sg.gov.dsta.ecs.sft.dao;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.quarkus.runtime.annotations.RegisterForReflection;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Map;

/*
 * SFT event
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@RegisterForReflection
public class EventStoreObject {
    @JsonProperty("UUID")
    private String uuid;
    @JsonProperty("BucketName")
    private String bucketName;
    @JsonProperty("BucketKey")
    private String bucketKey;
    @JsonProperty("Timestamp")
    private String ts;
    @JsonProperty("ProcessingStep")
    private String processingStep;
    @JsonProperty("ProcessingStatus")
    private String processingStatus;
    @JsonProperty("AppId")
    private String appId;
    @JsonProperty("ContentType")
    private String contentType;
    @JsonProperty("Verdict")
    private String verdict;
    @JsonProperty("Sha256")
    private String sha256;
    @JsonProperty("DataId")
    private String dataId;
    @JsonProperty("Callback")
    private String callback;
    @JsonProperty("FileStatus")
    private String fileStatus; // Unchanged, Sansitied, Quarantine

    private static final DateTimeFormatter sdf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public EventStoreObject() {
        this.ts = LocalDateTime.now(ZoneId.of("UTC+8")).format(sdf);
    }

    public EventStoreObject(String uuid, String bucketName, String bucketKey, String ts, String processingStep, String processingStatus, String appId, String contentType, String verdict, String sha256, String dataId) {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        this.ts = LocalDateTime.now(ZoneId.of("UTC+8")).format(sdf);
        this.uuid = uuid;
        this.bucketName = bucketName;
        this.bucketKey = bucketKey;
        this.processingStep = processingStep;
        this.processingStatus = processingStatus;
        this.appId = appId;
        this.contentType = contentType;
        this.verdict = verdict;
        this.sha256 = sha256;
        this.dataId = dataId;
    }

    public Map<String, Object> toMap() {
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> map = mapper.convertValue(this, Map.class); // convert class to a map
        map.put("pk", getBucketKey());
        return map;
    }

    public static String toJson(EventStoreObject eso) {
        ObjectMapper mapper = new ObjectMapper();
        String jo = null;
        try {
            jo = mapper.writeValueAsString(eso);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return jo;
    }

    public String getTs() {
        if (this.ts == null) {
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            this.ts = LocalDateTime.now(ZoneId.of("UTC+8")).format(sdf);
        }
        return this.ts;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    public String getBucketKey() {
        return bucketKey;
    }

    public void setBucketKey(String bucketKey) {
        this.bucketKey = bucketKey;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public String getProcessingStep() {
        return processingStep;
    }

    public void setProcessingStep(String processingStep) {
        this.processingStep = processingStep;
    }

    public String getProcessingStatus() {
        return processingStatus;
    }

    public void setProcessingStatus(String processingStatus) {
        this.processingStatus = processingStatus;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getVerdict() {
        return verdict;
    }

    public void setVerdict(String verdict) {
        this.verdict = verdict;
    }

    public String getSha256() {
        return sha256;
    }

    public void setSha256(String sha256) {
        this.sha256 = sha256;
    }

    public String getDataId() {
        return dataId;
    }

    public void setDataId(String dataId) {
        this.dataId = dataId;
    }

    public String getCallback() {
        return callback;
    }

    public void setCallback(String callback) {
        this.callback = callback;
    }

    public String getFileStatus() {
        return fileStatus;
    }

    public void setFileStatus(String fileStatus) {
        this.fileStatus = fileStatus;
    }


    public static final class Builder {
        private EventStoreObject eventStoreObject;

        private Builder() {
            eventStoreObject = new EventStoreObject();
        }

        public static Builder anEventStoreObject() {
            return new Builder();
        }

        public Builder withUuid(String uuid) {
            eventStoreObject.setUuid(uuid);
            return this;
        }

        public Builder withBucketName(String bucketName) {
            eventStoreObject.setBucketName(bucketName);
            return this;
        }

        public Builder withBucketKey(String bucketKey) {
            eventStoreObject.setBucketKey(bucketKey);
            return this;
        }

        public Builder withProcessingStep(String processingStep) {
            eventStoreObject.setProcessingStep(processingStep);
            return this;
        }

        public Builder withProcessingStatus(String processingStatus) {
            eventStoreObject.setProcessingStatus(processingStatus);
            return this;
        }

        public Builder withAppId(String appId) {
            eventStoreObject.setAppId(appId);
            return this;
        }

        public Builder withContentType(String contentType) {
            eventStoreObject.setContentType(contentType);
            return this;
        }

        public Builder withVerdict(String verdict) {
            eventStoreObject.setVerdict(verdict);
            return this;
        }

        public Builder withSha256(String sha256) {
            eventStoreObject.setSha256(sha256);
            return this;
        }

        public Builder withDataId(String dataId) {
            eventStoreObject.setDataId(dataId);
            return this;
        }

        public Builder withCallback(String callback) {
            eventStoreObject.setCallback(callback);
            return this;
        }

        public Builder withFileStatus(String fileStatus) {
            eventStoreObject.setFileStatus(fileStatus);
            return this;
        }

        public EventStoreObject build() {
            return eventStoreObject;
        }
    }

}