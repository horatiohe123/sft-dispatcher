package sg.gov.dsta.ecs.sft;

import java.net.URI;
import java.util.Objects;
import java.util.UUID;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.http.client.utils.URIBuilder;
import org.jboss.logging.Logger;
import org.json.JSONObject;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import sg.gov.dsta.ecs.sft.aws.S3Handler;
import sg.gov.dsta.ecs.sft.dao.EventStoreObject;
import sg.gov.dsta.ecs.sft.dao.ResponseErrorMessage;
import sg.gov.dsta.ecs.sft.dao.ResponseMessage;
import sg.gov.dsta.ecs.sft.service.AuditService;
import sg.gov.dsta.ecs.sft.service.Authorizer;
import sg.gov.dsta.ecs.sft.service.EventStoreService;
import sg.gov.dsta.ecs.sft.utils.CustomLogger;
import sg.gov.dsta.ecs.sft.utils.ResponseBuilder;
import sg.gov.dsta.sft.enums.FileStatus;
import sg.gov.dsta.sft.enums.ProcessingSteps;
import sg.gov.dsta.sft.enums.ScanResult;
import software.amazon.awssdk.http.HttpStatusCode;

@Named("main")
public class MainLambda
    implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {
  private static final Logger log = Logger.getLogger(MainLambda.class);
  private static final String APPLICATION_NAME = "SFT";
  private static final String LOG_FORMAT = "[Lambda Event]: %s";
  private static final String LANDING_BUCKET_NAME_KEY = "LANDING_BUCKET_NAME";
  private static final String SECURE_BUCKET_NAME_KEY = "SECURE_BUCKET_NAME";
  private String reverseProxy = System.getenv("REVERSE_PROXY");
  private String landingBucketName =
      Objects.requireNonNullElse(System.getenv(LANDING_BUCKET_NAME_KEY), "sft-landingzone");
  private String secureBucketName =
      Objects.requireNonNullElse(System.getenv(SECURE_BUCKET_NAME_KEY), "sft-securezone");


  @Inject
  public Authorizer authorizer;
  @Inject
  public EventStoreService eventStoreService;
  @Inject
  public AuditService auditService;
  @Inject
  public S3Handler s3;

  /**
   * Setting environment variables from environment json variable
   */
  public MainLambda() {
    String appJsonVariables = System.getenv("APP_VARIABLES");
    if (appJsonVariables != null && !appJsonVariables.equals("NA")) {
      var variables = new JSONObject(appJsonVariables);
      if (!variables.isNull(LANDING_BUCKET_NAME_KEY)) {
        landingBucketName = variables.getString(LANDING_BUCKET_NAME_KEY);
      }
      if (!variables.isNull(SECURE_BUCKET_NAME_KEY)) {
        secureBucketName = variables.getString(SECURE_BUCKET_NAME_KEY);
      }
    }
  }

  @Override
  public APIGatewayProxyResponseEvent handleRequest(APIGatewayProxyRequestEvent event,
      Context context) {

    var uuid = UUID.randomUUID().toString();
    APIGatewayProxyResponseEvent resp = null;
    ResponseErrorMessage rem = null;
    ResponseMessage rm = null;
    long startTime = System.currentTimeMillis();

    try {

      var body = new JSONObject(event.getBody());
      var appid = body.getString("appid");
      var fileName = body.getString("name");
      var action = body.getString("action");
      var contentType = body.getString("type");
      var preSignedUrl = "";
      JSONObject authJson = authorizer.auth(appid + "#" + contentType);

      if (authJson == null || authJson.getString("pkstatus").equals("false")) {

        var errMsg = "Not Authorised to perform file scanning";
        rem = ResponseErrorMessage.builder().withUuid(uuid).withProcessingStep("PreSignedUrl")
            .withMessage(errMsg).build();
        resp = ResponseBuilder.toApiGatewayResponse(rem, HttpStatusCode.UNAUTHORIZED);
        log.errorf(LOG_FORMAT, errMsg);
        return resp;
      }

      if (action.equals("put")) {

        var rpCallback = body.getString("callback");
        var objectKey = appid + "/" + uuid + "_" + fileName;
        preSignedUrl = s3.preSignedUrlPUT(landingBucketName, objectKey, contentType).toString();
        // swap host and endpoint with reverse proxy
        preSignedUrl = Objects.isNull(reverseProxy) ? preSignedUrl
            : new URIBuilder(URI.create(preSignedUrl)).setHost(reverseProxy + "/put").build()
                .toString();

        var eso = EventStoreObject.Builder.anEventStoreObject().withBucketKey(objectKey)
            .withBucketName(landingBucketName).withUuid(uuid)
            .withProcessingStep(ProcessingSteps.PUT_PRESIGNED_URL.getProcessingStep())
            .withProcessingStatus(ScanResult.PROCESSING.getScanResult()).withAppId(appid)
            .withCallback(rpCallback).withContentType(contentType)
            .withFileStatus(FileStatus.DEFAULT.getFileStatus()).build();
        eventStoreService.addEvent(eso);
        log.infov("EventStoreObject: %s", eso.toString());

        auditService
            .audit(CustomLogger.Builder.aCustomLogger().withApplicationName(APPLICATION_NAME)
                .withCorrelationID(objectKey).withUUID(UUID.randomUUID().toString())
                .withDuration(System.currentTimeMillis() - startTime).withEndpoint("/put")
                .withSourceApplicationName(appid).build());

        log.infof(LOG_FORMAT, "putPreSignedUrl generated successfully");
        rm = ResponseMessage.builder().withUuid(uuid).withProcessingStep(eso.getProcessingStep())
            .withProcessingStatus(eso.getProcessingStatus()).withPresignUrl(preSignedUrl)
            .withVerdicts("").withFileStatus(eso.getFileStatus()).build();

        resp = ResponseBuilder.toApiGatewayResponse(rm, HttpStatusCode.CREATED);


      } else if (action.equals("get")) {

        var requestId = body.getString("uuid");
        var objectKey = appid + "/" + requestId + "_" + fileName;
        EventStoreObject retrievedItem = eventStoreService.getEvent(objectKey);

        if (retrievedItem == null) {

          rem = ResponseErrorMessage.builder().withUuid(uuid)
              .withProcessingStep(ProcessingSteps.GETPRESIGNED_URL.getProcessingStep())
              .withMessage("can not find your record").build();
          resp = ResponseBuilder.toApiGatewayResponse(rem, HttpStatusCode.NO_CONTENT);
          return resp;
        }

        log.infov("Retrieved Item: %s", retrievedItem.toString());
        if (retrievedItem.getProcessingStatus().equals(ScanResult.ALLOWED.getScanResult())) {

          preSignedUrl = s3.preSignedUrlGET(secureBucketName, objectKey).toString();
          preSignedUrl = Objects.isNull(reverseProxy) ? preSignedUrl
              : new URIBuilder(URI.create(preSignedUrl)).setHost(reverseProxy + "/get").build()
                  .toString();

          eventStoreService.updateEvent(objectKey, EventStoreObject.Builder.anEventStoreObject()
              .withProcessingStep(ProcessingSteps.GETPRESIGNED_URL.getProcessingStep()).build());

          log.infof(LOG_FORMAT, "getPreSignedUrl generated successfully");

        } else {
          preSignedUrl = "";
        }

        rm = ResponseMessage.builder().withUuid(uuid)
            .withProcessingStep(ProcessingSteps.GETPRESIGNED_URL.getProcessingStep())
            .withProcessingStatus(retrievedItem.getProcessingStatus()).withPresignUrl(preSignedUrl)
            .withVerdicts(retrievedItem.getVerdict()).withFileStatus(retrievedItem.getFileStatus())
            .build();
        resp = ResponseBuilder.toApiGatewayResponse(rm, HttpStatusCode.CREATED);

      }
      return resp;

    } catch (Exception e) {

      log.errorf(LOG_FORMAT, e.getMessage());
      log.errorf(LOG_FORMAT, e.getCause());

      rem = ResponseErrorMessage.builder().withUuid(uuid).withProcessingStep("PreSignedUrl")
          .withMessage(e.getMessage()).build();
      resp = ResponseBuilder.toApiGatewayResponse(rem, HttpStatusCode.INTERNAL_SERVER_ERROR);
      return resp;
    }
  }


  public void audit(CustomLogger cl) {
    log.info("[SFT Auditting]: " + CustomLogger.toJson(cl));
  }

}
