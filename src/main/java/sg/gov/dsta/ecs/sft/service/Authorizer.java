package sg.gov.dsta.ecs.sft.service;

import org.jboss.logging.Logger;
import org.json.JSONObject;
import sg.gov.dsta.ecs.sft.aws.DynamoDBHandler;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.Map;

@ApplicationScoped
public class Authorizer {
    private static final Logger log = Logger.getLogger(Authorizer.class);
    private static final String LOG_FORMAT = "[SFT Authorizer]: %s";
    private static final String TABLE_NAME_TEMPLATE = "sft-authorizer";
    private String env;
    private String tableName;

    @Inject
    public DynamoDBHandler ddh;

    public Authorizer() {
        log.infof(LOG_FORMAT, "initializing...");
        env = System.getenv("ENVIRONMENT");
        if (env != null) {
            tableName = TABLE_NAME_TEMPLATE + "-" + env;
        } else {
            tableName = TABLE_NAME_TEMPLATE;
        }
    }

    public JSONObject auth(String pk) {

        log.infof(LOG_FORMAT, "loading auth info from: " + tableName);
        Map<String, Object> record = ddh.getItem(tableName, pk, null);
        if (record == null || record.isEmpty()) {
            log.infof(LOG_FORMAT, "Record not found in authorizer table");
            return null;
        } else {
            return new JSONObject(record);
        }
    }
}

