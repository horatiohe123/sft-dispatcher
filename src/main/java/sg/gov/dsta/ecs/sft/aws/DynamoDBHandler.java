package sg.gov.dsta.ecs.sft.aws;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import software.amazon.awssdk.http.urlconnection.UrlConnectionHttpClient;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.dynamodb.DynamoDbClient;
import software.amazon.awssdk.services.dynamodb.model.AttributeAction;
import software.amazon.awssdk.services.dynamodb.model.AttributeValue;
import software.amazon.awssdk.services.dynamodb.model.AttributeValueUpdate;
import software.amazon.awssdk.services.dynamodb.model.DeleteItemRequest;
import software.amazon.awssdk.services.dynamodb.model.DynamoDbException;
import software.amazon.awssdk.services.dynamodb.model.GetItemRequest;
import software.amazon.awssdk.services.dynamodb.model.PutItemRequest;
import software.amazon.awssdk.services.dynamodb.model.ResourceNotFoundException;
import software.amazon.awssdk.services.dynamodb.model.UpdateItemRequest;

import javax.enterprise.context.ApplicationScoped;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

@ApplicationScoped
public class DynamoDBHandler {
    private final Logger log = LoggerFactory.getLogger(DynamoDBHandler.class);
    DynamoDbClient ddb;

    public DynamoDBHandler() {
        Region region = Region.AP_SOUTHEAST_1;
        ddb = DynamoDbClient.builder()
                .region(region)
                .httpClient(UrlConnectionHttpClient.builder().build())
                .build();
    }

    Marker marker;

    public Map<String, Object> getItem(String tableName,
                                       String pkValue,
                                       String skValue) {
        Map<String, AttributeValue> record;
        HashMap<String, AttributeValue> keyToGet = new HashMap<>();
        //add pk
        keyToGet.put("pk", AttributeValue.builder().s(pkValue).build());
        //add sk
        if (skValue != null) {
            keyToGet.put("sk", AttributeValue.builder().s(skValue).build());
        }
        log.info(marker, "[DynamoDBHandler]: pk value is {}", keyToGet);
        //create getItemRequest
        GetItemRequest request = GetItemRequest.builder()
                .key(keyToGet)
                .tableName(tableName)
                .build();
        try {
            record = ddb.getItem(request).item();
            log.info(marker, "[DynamoDBHandler]: Amazon DynamoDB table attributes: {}", record.keySet());
            return map(record);
        } catch (ResourceNotFoundException e) {
            log.error(marker, "[DynamoDBHandler]: Table not found {} \n Error Message: {}", tableName, e.getMessage());
            return null;
        } catch (DynamoDbException e) {
            log.error(marker, "[DynamoDBHandler]: DynamoDB Processing error {}", e.getMessage());
            return null;
        }
    }

    public void putItem(String tableName, Map<String, Object> itemValues) {
        PutItemRequest request = PutItemRequest.builder()
                .tableName(tableName)
                .item(build(itemValues))
                .build();
        try {
            ddb.putItem(request);
        } catch (ResourceNotFoundException e) {
            log.error(marker, "[DynamoDBHandler]: Error: The Amazon DynamoDB table \"%s\" can't be found.\n {}", tableName);
            log.error("[DynamoDBHandler]: Be sure that it exists and that you've typed its name correctly!");
        } catch (DynamoDbException e) {
            log.error(marker, "[DynamoDBHandler]: DynamoDB Exception {}", e.getMessage());
        }
    }

    public void updateTableItem(String tableName, String pkValue,
                                String skValue,
                                String name,
                                String updateVal) {
        Map<String, AttributeValue> keys = new TreeMap<>();
        Map<String, AttributeValueUpdate> updatedValues = new TreeMap<>();
        //add pk
        keys.put("pk", AttributeValue.builder().s(pkValue).build());
        //add sk
        if (skValue != null) {
            keys.put("sk", AttributeValue.builder().s(skValue).build());
        }
        // Update the column specified by name with updatedVal
        updatedValues.put(name, AttributeValueUpdate.builder()
                .value(AttributeValue.builder().s(updateVal).build())
                .action(AttributeAction.PUT)
                .build());
        UpdateItemRequest request = UpdateItemRequest.builder()
                .tableName(tableName)
                .key(keys)
                .attributeUpdates(updatedValues)
                .build();
        try {
            ddb.updateItem(request);
        } catch (DynamoDbException e) {
            log.error(marker, "[DynamoDBHandler Update]: update error {}", e.getMessage());
        }
    }

    public Map<String, Object> updateTableItems(String tableName, String pkValue,
                                                String skValue, Map<String, Object> updates) {
        Map<String, AttributeValue> keys = new TreeMap<>();
        Map<String, AttributeValueUpdate> updatedValues = new TreeMap<>();
        //add pk
        keys.put("pk", AttributeValue.builder().s(pkValue).build());
        //add sk
        if (skValue != null) {
            keys.put("sk", AttributeValue.builder().s(skValue).build());
        }

        // Update the column specified by name with updatedVal
        for (Map.Entry<String, Object> entry : updates.entrySet()) {
            if (entry.getValue() != null) {
                updatedValues.put(entry.getKey(), AttributeValueUpdate.builder()
                        .value(build(entry.getValue()))
                        .action(AttributeAction.PUT)
                        .build());
            }
        }

        UpdateItemRequest request = UpdateItemRequest.builder()
                .tableName(tableName)
                .key(keys)
                .attributeUpdates(updatedValues)
                .build();
        try {
            return map(ddb.updateItem(request).attributes());
        } catch (DynamoDbException e) {
            log.error(marker, "[DynamoDBHandler Update]: update error {}", e.getMessage());
            return null;
        }
    }

    public void deleteItem(String tableName, String pkValue, String skValue) {
        Map<String, AttributeValue> keyToGet = new TreeMap<>();
        //add pk
        keyToGet.put("appID", AttributeValue.builder().s(pkValue).build());
        //add sk
        if (skValue != null) {
            keyToGet.put("sk", AttributeValue.builder().s(skValue).build());
        }
        try {
            ddb.deleteItem(DeleteItemRequest
                    .builder()
                    .tableName(tableName)
                    .key(keyToGet)
                    .build());
        } catch (DynamoDbException e) {
            log.error(marker, "[DynamoDBHandler Delete]: delete error {}", e.getMessage());
        }
    }

    /*======================================= Auxiliary Function ======================================= */
    public Map<String, Object> map(Map<String, AttributeValue> original) {
        Map<String, Object> output = new HashMap<>();
        for (String key : original.keySet()) {
            output.put(key, map(original.get(key)));
        }
        return output;
    }

    public List<Object> map(List<AttributeValue> original) {
        List<Object> output = new ArrayList<>();
        for (AttributeValue value : original) {
            output.add(map(value));
        }
        return output;
    }

    public Object map(AttributeValue original) {
        Object output;
        if (original.hasM()) {
            return map(original.m());
        } else if (original.hasL()) {
            output = map(original.l());
        } else if (original.hasSs()) {
            output = original.ss();
        } else if (original.hasNs()) {
            output = original.ns();
        } else if (original.n() != null) {
            output = original.n();
        } else if (original.s() != null) {
            output = original.s();
        } else if (original.bool() != null) {
            output = original.bool();
        } else {
            log.error(marker, "Unable to convert: {}", original);
            output = original.toString();
        }
        return output;
    }

    public Map<String, AttributeValue> build(Map<String, Object> original) {
        Map<String, AttributeValue> output = new HashMap<>();
        for (String key : original.keySet()) {
            if (original.get(key) != null) {
                output.put(key, build(original.get(key)));
            }
        }
        return output;
    }

    public List<AttributeValue> build(List<Object> original) {
        List<AttributeValue> output = new ArrayList<>();
        for (Object value : original) {
            if (value != null) {
                output.add(build(value));
            }
        }
        return output;
    }

    public AttributeValue build(Object original) {
        AttributeValue output = null;
        if (original instanceof Map) {
            Map<String, Object> mapOriginal = (Map) original;
            return AttributeValue.builder().m(build(mapOriginal)).build();
        } else if (original instanceof List) {
            List<Object> listOriginal = (List) original;
            output = AttributeValue.builder().l(build(listOriginal)).build();
        } else if (original instanceof Set) {
            Set setOriginal = (Set) original;
            if (setOriginal.contains(String.class))
                output = AttributeValue.builder().ss(setOriginal).build();
            else if (setOriginal.contains(Integer.class))
                output = build(AttributeValue.builder().ns(setOriginal).build());
        } else if (original instanceof Number) {
            Number num = (Number) original;
            output = AttributeValue.builder().n(num.toString()).build();
        } else if (original instanceof String) {
            String stringOriginal = (String) original;
            try {
                int testInt = Integer.parseInt(stringOriginal);
                output = AttributeValue.builder().n(testInt + "").build();
            } catch (NumberFormatException e) {
                output = AttributeValue.builder().s(stringOriginal).build();
            }
        } else if (original instanceof Boolean) {
            Boolean booleanOriginal = (Boolean) original;
            output = AttributeValue.builder().bool(booleanOriginal).build();
        } else {
            log.error(marker, "Unable to convert: {}", original.toString());
            output = AttributeValue.builder().s(original.toString()).build();
        }
        return output;
    }

}
