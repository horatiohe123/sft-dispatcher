package sg.gov.dsta.ecs.sft.utils;

import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * To build lambda response
 */
public class ResponseBuilder {
  private ResponseBuilder() {}

  /**
   * @param status Response body status
   * @param statusCode HTTP status code
   * @param uuid Correlation id of the request
   * @param message Response body message
   * @return An ApiGatewayProxyResponseEvent which represent HTTP
   */
  public static APIGatewayProxyResponseEvent toApiGatewayResponse(String status, int statusCode,
      String uuid, String message) {
    var resp = new APIGatewayProxyResponseEvent();
    Map<String, String> responseHeaders = new HashMap<>();
    responseHeaders.put("Content-Type", "application/json");
    responseHeaders.put("X-Custom-Header", "application/json");
    responseHeaders.put("Access-Control-Allow-Origin", "*");
    responseHeaders.put("Access-Control-Allow-Credentials", "true");
    responseHeaders.put("Access-Control-Allow-Methods", "POST, GET, OPTIONS");

    var responseBody = new JSONObject();
    responseBody.put("Status", status);
    responseBody.put("Message", message);
    responseBody.put("UUID", uuid);
    resp.setBody(responseBody.toString());
    resp.setStatusCode(statusCode);
    resp.setIsBase64Encoded(false);
    resp.setHeaders(responseHeaders);
    return resp;
  }

  public static APIGatewayProxyResponseEvent toApiGatewayResponse(Object obj, int statusCode) {
    var resp = new APIGatewayProxyResponseEvent();
    Map<String, String> responseHeaders = new HashMap<>();
    responseHeaders.put("Content-Type", "application/json");
    responseHeaders.put("X-Custom-Header", "application/json");
    responseHeaders.put("Access-Control-Allow-Origin", "*");
    responseHeaders.put("Access-Control-Allow-Credentials", "true");
    responseHeaders.put("Access-Control-Allow-Methods", "POST, GET, OPTIONS");

    resp.setBody(toJsonString(obj));
    resp.setStatusCode(statusCode);
    resp.setIsBase64Encoded(false);
    resp.setHeaders(responseHeaders);
    return resp;
  }

  private static String toJsonString(Object obj) {

    var mapper = new ObjectMapper();
    var result = "";
    try {
      result = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(obj);
    } catch (JsonProcessingException e) {

      result = obj.toString();
    }
    return result;

  }
}
