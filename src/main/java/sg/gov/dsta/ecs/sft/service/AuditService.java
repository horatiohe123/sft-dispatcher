package sg.gov.dsta.ecs.sft.service;

import org.jboss.logging.Logger;
import sg.gov.dsta.ecs.sft.aws.DynamoDBHandler;
import sg.gov.dsta.ecs.sft.aws.SQSHandler;
import sg.gov.dsta.ecs.sft.utils.CustomLogger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

/**
 * Handles the audit trail and logging to DynamoDB audit table and SQS
 */
@ApplicationScoped
public class AuditService {
    private static final Logger log = Logger.getLogger(AuditService.class);
    private static final String TABLE_NAME_TEMPLATE = "sft-audit";
    private String env;
    private String tableName;

    @Inject
    public DynamoDBHandler ddh;

    public AuditService() {
        env = System.getenv("ENVIRONMENT");
        if (env != null) {
            tableName = TABLE_NAME_TEMPLATE + "-" + env;
        } else {
            tableName = TABLE_NAME_TEMPLATE;
        }
    }

    public void audit(CustomLogger cl) {
        ddh.putItem(tableName, cl.toMap());
        log.info("[SFT Auditting]: " + CustomLogger.toJson(cl));
    }

}
