package sg.gov.dsta.ecs.sft.aws;

import org.jboss.logging.Logger;
import software.amazon.awssdk.core.ResponseBytes;
import software.amazon.awssdk.core.exception.SdkClientException;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.http.urlconnection.UrlConnectionHttpClient;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.DeleteObjectRequest;
import software.amazon.awssdk.services.s3.model.GetObjectRequest;
import software.amazon.awssdk.services.s3.model.GetObjectResponse;
import software.amazon.awssdk.services.s3.model.ObjectLockLegalHoldStatus;
import software.amazon.awssdk.services.s3.model.ObjectLockMode;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;
import software.amazon.awssdk.services.s3.model.PutObjectResponse;
import software.amazon.awssdk.services.s3.presigner.S3Presigner;
import software.amazon.awssdk.services.s3.presigner.model.GetObjectPresignRequest;
import software.amazon.awssdk.services.s3.presigner.model.PresignedGetObjectRequest;
import software.amazon.awssdk.services.s3.presigner.model.PresignedPutObjectRequest;
import software.amazon.awssdk.services.s3.presigner.model.PutObjectPresignRequest;

import javax.enterprise.context.ApplicationScoped;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.time.Instant;

@ApplicationScoped
public class S3Handler {
    private static final Logger log = Logger.getLogger(S3Handler.class);

    S3Client s3Client;

    public S3Handler() {
        Region region = Region.AP_SOUTHEAST_1;
        s3Client = S3Client.builder()
                .region(region)
                .httpClient(UrlConnectionHttpClient.builder().build())
                .build();
    }

    public boolean putObject(String bucketName, String objectKey, byte[] fileContent) {
        try {
            // Upload a text string as a new object.
//            * this is v2 function *
            PutObjectResponse putObjectResponse = s3Client.putObject(PutObjectRequest.builder().bucket(bucketName).key(objectKey).build(), RequestBody.fromBytes(fileContent));
            log.info("[S3Handler CreateObject]: " + objectKey + " has been uploaded..." + putObjectResponse.sdkHttpResponse().isSuccessful());
            return putObjectResponse.sdkHttpResponse().isSuccessful();
        } catch (Exception e) {
            // Amazon S3 couldn't be contacted for a response, or the client
            // couldn't parse the response from Amazon S3.
            log.error("[S3Handler CreateObject]: " + e.getMessage());
            return false;
        }
    }

    public URL preSignedUrlGET(String bucketName, String objectKey) {
        URL url = null;
        try (S3Presigner s3Presigner = S3Presigner.create()) {
            // Generate the presigned URL.
            log.info("[S3Handler GetPreSignedUrl]: Generating pre-signed URL.");
            GetObjectRequest objectRequest = GetObjectRequest.builder()
                    .bucket(bucketName)
                    .key(objectKey)
                    .build();

            GetObjectPresignRequest presignRequest = GetObjectPresignRequest.builder()
                    .signatureDuration(Duration.ofMinutes(5))
                    .getObjectRequest(objectRequest)
                    .build();

            PresignedGetObjectRequest presignedRequest = s3Presigner.presignGetObject(presignRequest);
            url = presignedRequest.url();
            log.info("[S3Handler GetPreSignedUrl]: Pre-Signed URL: " + url.toString());

        } catch (Exception e) {
            // Amazon S3 couldn't be contacted for a response, or the client
            // couldn't parse the response from Amazon S3.
            log.error("[S3Handler GetPreSignedUrl]: " + e.getMessage());
        }
        return url;
    }

    public URL preSignedUrlPUT(String bucketName, String objectKey, String contentType) {
        URL url = null;
        try (S3Presigner s3Presigner = S3Presigner.create()) {
            // Generate the presigned URL.
            log.info("[S3Handler PutPreSignedUrl]: Generating pre-signed URL.");
            PutObjectRequest objectRequest = PutObjectRequest.builder()
                    .bucket(bucketName)
                    .key(objectKey)
//                    .objectLockLegalHoldStatus(ObjectLockLegalHoldStatus.ON)
//                    .objectLockMode(ObjectLockMode.GOVERNANCE)
//                    .objectLockRetainUntilDate(Instant.now().plusSeconds(300))
                    .contentType(contentType)
                    .build();

            PutObjectPresignRequest presignRequest = PutObjectPresignRequest.builder()
                    .signatureDuration(Duration.ofMinutes(5))
                    .putObjectRequest(objectRequest)
                    .build();

            PresignedPutObjectRequest presignedRequest = s3Presigner.presignPutObject(presignRequest);
            url = presignedRequest.url();
            log.info("[S3Handler PutPreSignedUrl]: Pre-Signed URL: " + url.toString());

        } catch (Exception e) {
            // Amazon S3 couldn't be contacted for a response, or the client
            // couldn't parse the response from Amazon S3.
            log.error("[S3Handler PutPreSignedUrl]: " + e.getMessage());
        }
        return url;
    }

    public int uploadWithPreSignedUrl(String bucketName, String objectKey, String type, byte[] fileContent) {
        int result = 0;
        try {
            // Create the connection and use it to upload the new object using the pre-signed URL.
            //create dummy object first
            byte[] tempContent = "temp file".getBytes(StandardCharsets.UTF_8);
            putObject(bucketName, objectKey, tempContent);
            //check the incoming file size
            if (fileContent.length <= 5000000) {
                URL preSignedUrl = preSignedUrlPUT(bucketName, objectKey, type);
                log.info("[S3Handler PreSignedURLPUT]: " + preSignedUrl.toString());
                HttpURLConnection connection = (HttpURLConnection) preSignedUrl.openConnection();
                connection.setDoOutput(true);
                connection.setRequestMethod("PUT");
                connection.setRequestProperty("Content-Type", type);
                OutputStream out = connection.getOutputStream();
                out.write(fileContent);
                out.close();

                // Check the HTTP response code. To complete the upload and make the object available,
                // you must interact with the connection object in some way.
                result = connection.getResponseCode();
                String response = connection.getResponseMessage();

                log.info("[S3Handler Upload]: HTTP response code: " + connection.getResponseCode());
                log.info("[S3Handler Upload]: HTTP response body: " + response);
                // Check to make sure that the object was uploaded successfully.
            } else {
                result = 401;
                log.error("[S3Handler Upload]: Rejected, File size is more than 5MB ");
            }
        } catch (Exception e) {
            deleteObject(bucketName, objectKey);
            log.error("[S3Handler Upload]: " + e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

    public byte[] getObject(String bucketName, String objectKey) {
        try {
            // Delete the sample objects.
            // Verify that the objects were deleted successfully.
            GetObjectRequest getObjectRequest = GetObjectRequest.builder()
                    .bucket(bucketName)
                    .key(objectKey)
                    .build();
            ResponseBytes<GetObjectResponse> objectBytes = s3Client.getObjectAsBytes(getObjectRequest);
            byte[] data = objectBytes.asByteArray();
            log.info("[S3Handler Get]: object successfully grabbed.");
            return data;
        } catch (SdkClientException e) {
            // Amazon S3 couldn't be contacted for a response, or the client
            // couldn't parse the response from Amazon S3.
            log.error("[S3Handler Delete]:" + e.getMessage());
            e.printStackTrace();
            return new byte[0];
        }
    }

    public int deleteObject(String bucketName, String objectKey) {
        try {
            // Delete the sample objects.
            // Verify that the objects were deleted successfully.
            DeleteObjectRequest deleteObjectRequest = DeleteObjectRequest.builder()
                    .bucket(bucketName)
                    .key(objectKey)
//                    .bypassGovernanceRetention(true) // Ignore object lock
                    .build();
            s3Client.deleteObject(deleteObjectRequest);
            log.info("[S3Handler Delete]: object successfully deleted.");
            return 0;
        } catch (SdkClientException e) {
            // Amazon S3 couldn't be contacted for a response, or the client
            // couldn't parse the response from Amazon S3.
            log.error("[S3Handler Delete]:" + e.getMessage());
            e.printStackTrace();
            return -1;
        }
    }

}
