package sg.gov.dsta.ecs.sft.dao;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.quarkus.runtime.annotations.RegisterForReflection;

@JsonIgnoreProperties(ignoreUnknown = true)
@RegisterForReflection
public class ResponseErrorMessage {

  @JsonProperty("Uuid")
  String uuid;
  @JsonProperty("ProcessingStep")
  String processingStep;
  @JsonProperty("Message")
  String message;

  private ResponseErrorMessage(Builder builder) {
    this.uuid = builder.uuid;
    this.processingStep = builder.processingStep;
    this.message = builder.message;
  }

  public ResponseErrorMessage() {}

  public String getUuid() {
    return uuid;
  }

  public String getProcessingStep() {
    return processingStep;
  }

  public String getMessage() {
    return message;
  }

  public void setUuid(String uuid) {
    this.uuid = uuid;
  }

  public void setProcessingStep(String processingStep) {
    this.processingStep = processingStep;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  @Override
  public String toString() {
    return "ResponseErrorMessage [uuid=" + uuid + ", processingStep=" + processingStep
        + ", message=" + message + "]";
  }

  /**
   * Creates builder to build {@link ResponseErrorMessage}.
   * 
   * @return created builder
   */
  public static Builder builder() {
    return new Builder();
  }

  /**
   * Builder to build {@link ResponseErrorMessage}.
   */
  public static final class Builder {
    private String uuid;
    private String processingStep;
    private String message;

    private Builder() {}

    public Builder withUuid(String uuid) {
      this.uuid = uuid;
      return this;
    }

    public Builder withProcessingStep(String processingStep) {
      this.processingStep = processingStep;
      return this;
    }

    public Builder withMessage(String message) {
      this.message = message;
      return this;
    }

    public ResponseErrorMessage build() {
      return new ResponseErrorMessage(this);
    }
  }

}
