package sg.gov.dsta.ecs.sft.aws;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

/**
 * Unit test for DynamoDBHandler module
 *
 * @see DynamoDBHandler
 */
@Disabled
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class DynamoDBHandlerTest {
    private DynamoDBHandler ddh;

    @BeforeAll
    public void init() {
        ddh = new DynamoDBHandler();
    }

    @Test
    public void getItem_Success() {
        String item = ddh.getItem("sft_authorizer", "onens#textplain", null).toString();
        Assertions.assertNotNull(item);
    }
}
