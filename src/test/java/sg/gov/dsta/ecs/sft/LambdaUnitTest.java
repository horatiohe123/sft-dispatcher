package sg.gov.dsta.ecs.sft;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;
import java.util.Collections;
import java.util.Map;
import javax.inject.Inject;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import sg.gov.dsta.ecs.sft.aws.DynamoDBHandler;
import sg.gov.dsta.ecs.sft.aws.S3Handler;
import sg.gov.dsta.ecs.sft.dao.EventStoreObject;
import sg.gov.dsta.ecs.sft.service.Authorizer;
import sg.gov.dsta.ecs.sft.service.EventStoreService;

@QuarkusTest
public class LambdaUnitTest {

  @Inject
  MainLambda mainLambda;

  @InjectMock
  Authorizer authorizer;

  @InjectMock
  EventStoreService ess;

  @InjectMock
  DynamoDBHandler ddh;

  @InjectMock
  S3Handler s3;

  @Test
  public void testPutSuccess() throws MalformedURLException {
    Context context = mock(Context.class);
    APIGatewayProxyRequestEvent event = mock(APIGatewayProxyRequestEvent.class);

    when(event.getBody()).thenReturn(
        "{\"appid\":\"testAppId\",\"name\":\"testName\",\"action\":\"put\",\"type\":\"testType\",\"callback\":\"testCallback\"}");
    JSONObject mockedJson = new JSONObject("{\"pkstatus\":\"true\"}");

    when(authorizer.auth("testAppId#testType")).thenReturn(mockedJson);

    final URLConnection mockConnection = Mockito.mock(URLConnection.class);
    final URLStreamHandler handler = new URLStreamHandler() {

      @Override
      protected URLConnection openConnection(final URL arg0) throws IOException {
        return mockConnection;
      }
    };
    final URL mockedUrl = new URL("http://mock.url", "mock.url", 80, "", handler);

    when(s3.preSignedUrlPUT(Mockito.any(String.class), Mockito.any(String.class),
        Mockito.any(String.class))).thenReturn(mockedUrl);

    assertEquals(201, mainLambda.handleRequest(event, context).getStatusCode());
  }

  @Test
  public void testGetSuccessDynamoDbItemExist() throws MalformedURLException {
    Context context = mock(Context.class);
    APIGatewayProxyRequestEvent event = mock(APIGatewayProxyRequestEvent.class);

    when(event.getBody()).thenReturn(
        "{\"appid\":\"testAppId\",\"name\":\"testName\",\"action\":\"get\",\"type\":\"testType\",\"uuid\":\"testUuid\"}");
    JSONObject mockedJson = new JSONObject("{\"pkstatus\":\"true\"}");

    when(authorizer.auth("testAppId#testType")).thenReturn(mockedJson);

    EventStoreObject storedEvent = new EventStoreObject();
    storedEvent.setProcessingStatus("Allowed");
    when(ess.getEvent(Mockito.any(String.class))).thenReturn(storedEvent);

    final URLConnection mockConnection = Mockito.mock(URLConnection.class);
    final URLStreamHandler handler = new URLStreamHandler() {

      @Override
      protected URLConnection openConnection(final URL arg0) throws IOException {
        return mockConnection;
      }
    };
    final URL mockedUrl = new URL("http://mock.url", "mock.url", 80, "", handler);

    when(s3.preSignedUrlGET(Mockito.any(String.class), Mockito.any(String.class)))
        .thenReturn(mockedUrl);

    assertEquals(201, mainLambda.handleRequest(event, context).getStatusCode());
  }

  @Test
  public void testGetFailDynamoDbItemExist() throws MalformedURLException {
    Context context = mock(Context.class);
    APIGatewayProxyRequestEvent event = mock(APIGatewayProxyRequestEvent.class);

    when(event.getBody()).thenReturn(
        "{\"appid\":\"testAppId\",\"name\":\"testName\",\"action\":\"get\",\"type\":\"testType\",\"uuid\":\"testUuid\"}");
    JSONObject mockedJson = new JSONObject("{\"pkstatus\":\"true\"}");

    when(authorizer.auth("testAppId#testType")).thenReturn(mockedJson);


    EventStoreObject storedEvent = new EventStoreObject();
    storedEvent.setProcessingStatus("Not Allowed");
    when(ess.getEvent(Mockito.any(String.class))).thenReturn(storedEvent);

    final URLConnection mockConnection = Mockito.mock(URLConnection.class);
    final URLStreamHandler handler = new URLStreamHandler() {

      @Override
      protected URLConnection openConnection(final URL arg0) throws IOException {
        return mockConnection;
      }
    };
    final URL mockedUrl = new URL("http://mock.url", "mock.url", 80, "", handler);

    when(s3.preSignedUrlGET(Mockito.any(String.class), Mockito.any(String.class)))
        .thenReturn(mockedUrl);

    assertEquals(201, mainLambda.handleRequest(event, context).getStatusCode());
  }

  @Test
  public void testGetFailDynamoDbItemDoesNotExist() throws MalformedURLException {
    Context context = mock(Context.class);
    APIGatewayProxyRequestEvent event = mock(APIGatewayProxyRequestEvent.class);

    when(event.getBody()).thenReturn(
        "{\"appid\":\"testAppId\",\"name\":\"testName\",\"action\":\"get\",\"type\":\"testType\",\"uuid\":\"testUuid\"}");
    JSONObject mockedJson = new JSONObject("{\"pkstatus\":\"true\"}");

    when(authorizer.auth("testAppId#testType")).thenReturn(mockedJson);

    Map<String, Object> map = Collections.emptyMap();

    when(ddh.getItem(Mockito.any(String.class), Mockito.any(String.class),
        Mockito.any(String.class))).thenReturn(map);

    final URLConnection mockConnection = Mockito.mock(URLConnection.class);
    final URLStreamHandler handler = new URLStreamHandler() {

      @Override
      protected URLConnection openConnection(final URL arg0) throws IOException {
        return mockConnection;
      }
    };
    final URL mockedUrl = new URL("http://mock.url", "mock.url", 80, "", handler);

    when(s3.preSignedUrlGET(Mockito.any(String.class), Mockito.any(String.class)))
        .thenReturn(mockedUrl);

    assertEquals(204, mainLambda.handleRequest(event, context).getStatusCode());
  }

  @Test
  public void testPkStatusFalse() {
    Context context = mock(Context.class);
    APIGatewayProxyRequestEvent event = mock(APIGatewayProxyRequestEvent.class);

    when(event.getBody()).thenReturn(
        "{\"appid\":\"testAppId\",\"name\":\"testName\",\"action\":\"put\",\"type\":\"testType\"}");
    JSONObject mockedJson = new JSONObject("{\"pkstatus\":\"false\"}");

    when(authorizer.auth("testAppId#testType")).thenReturn(mockedJson);

    assertEquals(401, mainLambda.handleRequest(event, context).getStatusCode());
  }

  @Test
  public void testAppUnauthorizedForContentType() {
    Context context = mock(Context.class);
    APIGatewayProxyRequestEvent event = mock(APIGatewayProxyRequestEvent.class);

    when(event.getBody()).thenReturn(
        "{\"appid\":\"testAppId\",\"name\":\"testName\",\"action\":\"put\",\"type\":\"testUnauthorizedType\"}");

    when(authorizer.auth("testAppId#testUnauthorizedType")).thenReturn(null);

    assertEquals(401, mainLambda.handleRequest(event, context).getStatusCode());
  }
}
