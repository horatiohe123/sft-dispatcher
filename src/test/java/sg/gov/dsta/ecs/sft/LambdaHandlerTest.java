package sg.gov.dsta.ecs.sft;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.quarkus.amazon.lambda.test.LambdaClient;
import io.quarkus.test.junit.QuarkusTest;

/**
 * Lambda end-to-end test with AWS integration
 */
@Disabled
@QuarkusTest
public class LambdaHandlerTest {

  @Test
  void testPutLambdaSuccess() throws Exception {
    ObjectMapper mapper = new ObjectMapper();
    APIGatewayProxyRequestEvent in = mapper.readValue(
        getClass().getClassLoader().getResourceAsStream("payload-put.json").readAllBytes(),
        APIGatewayProxyRequestEvent.class);
    APIGatewayProxyResponseEvent out = LambdaClient.invoke(APIGatewayProxyResponseEvent.class, in);
    System.out.println(out.getBody());
    Assertions.assertTrue(true);
  }

  @Test
  void testGetLambdaSuccess2() throws Exception {
    ObjectMapper mapper = new ObjectMapper();
    APIGatewayProxyRequestEvent in = mapper.readValue(
        getClass().getClassLoader().getResourceAsStream("payload-get.json").readAllBytes(),
        APIGatewayProxyRequestEvent.class);
    APIGatewayProxyResponseEvent out = LambdaClient.invoke(APIGatewayProxyResponseEvent.class, in);
    System.out.println(out.getBody());
    Assertions.assertTrue(true);
  }

}
