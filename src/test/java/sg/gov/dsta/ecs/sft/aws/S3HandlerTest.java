package sg.gov.dsta.ecs.sft.aws;

import org.jboss.logging.Logger;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.wildfly.common.Assert;

import java.net.URL;

/**
 * Unit test for S3Handler module
 *
 * @see S3Handler
 */
@Disabled
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class S3HandlerTest {
    private static final Logger log = Logger.getLogger(S3HandlerTest.class);
    private static final String TEST_BUCKET_NAME = "sft-landingzone";
    private static final String TEST_KEY = "smartict/testing1.txt";
    private S3Handler s3Handler;

    /**
     * Inits the S3Handler before each test case
     */
    @BeforeAll
    public void init() {
        this.s3Handler = new S3Handler();
    }

    @Test
    void getPreSignedUrlPut_Success() {
        URL url = this.s3Handler.preSignedUrlPUT(TEST_BUCKET_NAME, TEST_KEY, "text/plain");
        System.out.println(url.toString());
        Assert.assertNotNull(url);
    }

    @Test
    void getPreSignedUrlGet_Success() {
        URL url = this.s3Handler.preSignedUrlGET(TEST_BUCKET_NAME, TEST_KEY);
        System.out.println(url.toString());
        Assert.assertNotNull(url);
    }

    @Test
    void deleteS3Bucket_Success() {
        int outcome = this.s3Handler.deleteObject(TEST_BUCKET_NAME, TEST_KEY);
        Assert.assertTrue(outcome == 0);
    }


//    public static void main(String[] args) {
//        //        System.out.println("[Secret Manager Checkup]:" + SecretManagerHandler.getSecret("dev/ecs/mdsdatastore"));
//        //get incoming file
//
//        try {
//            Path path = Paths.get("/Users/horatio/Downloads/test.txt");
//            byte[] data = Files.readAllBytes(path);
//            String dataString = Base64.getEncoder().encodeToString(data);
//            System.out.println(dataString);
//            String fileName = "coffee-brochure.pdf";
//            String appId = "onens";
//            String bucketName = "sft-landingzone";
////            S3Handler s3 = new S3Handler();
////            int result = s3.uploadWithPreSignedUrl("sft-landingzone",appId+"/"+fileName, "application/pdf", data);
////            log.info("[S3 Tester]: " + result);
////            s3.deleteObject(bucketName,appId+"/"+fileName);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

//    }
}